var mainApp = angular.module("mainApp", ['ngRoute']);
mainApp.config(['$routeProvider', function($routeProvider) {
            $routeProvider

            .when('/home', {
               templateUrl: 'src/views/home.htm',
               controller: 'HomeController'
            })
            .otherwise({
               redirectTo: '/home'
            });
         }]);
